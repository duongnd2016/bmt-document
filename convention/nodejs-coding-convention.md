# NodeJS Coding Conventions

## **1. Source file basics**

### **1.1. File name**

File names must be all lowercase and may include dashes (-), but no additional punctuation. Filenames’ extension must be .js or .ts

### **1.2. File encoding**

Source files are encoded in UTF-8.

### **1.3. Special characters**

### 1.3.1. Whitespace characters

Aside from the line terminator sequence, the ASCII horizontal space character (0x20) is the only whitespace character that appears anywhere in a source file. This implies that
  1. All other whitespace characters in string literals are escaped, and
  2. Tab characters are not used for indentation.

### 1.3.2. Special escape sequences

For any character that has a special escape sequence (\', \", \\, \b, \f, \n, \r, \t, \v), that sequence is used rather than the corresponding numeric escape (e.g \x0a, \u000a, or \u{a}). Legacy octal escapes are never used.

### 1.3.3. Non-ASCII characters

For the remaining non-ASCII characters, either the actual Unicode character (e.g. ∞) or the equivalent hex or Unicode escape (e.g. \u221e) is used, depending only on which makes the code easier to read and understand.

| Example                                          | Discussion                                                                |
| :--------------------------- | :----------------------------------------------- |
| const units = 'μs';                              | Best: perfectly clear even without a comment.                             |
| const units = '\u03bcs'; // 'μs'                 | Allowed, but there’s no reason to do this.                               |
| const units = '\u03bcs'; // Greek letter mu, 's' | Allowed, but awkward and prone to mistakes.                               |
| const units = '\u03bcs';                         | Poor: the reader has no idea what this is.                                |
| return '\ufeff' + content; // byte order mark    | Good: use escapes for non-printable characters, and comment if necessary. |

## **2. Formating**

### **2.1. Braces**

Braces are required for all control structures (i.e. `if`, `else`, `for`, `do`, `while`, as well as any others), even if the body contains only a single statement. The first statement of a non-empty block must begin on its own line.

Illegal:
```
if (someVeryLongCondition())
  doSomething();

for (let i = 0; i < foo.length; i++) bar(foo[i]);

```
Nonempty blocks: 

K&R style (Braces follow the Kernighan and Ritchie style (Egyptian brackets) for nonempty blocks and block-like constructs)
  1. No line break before the opening brace.
  2. Line break after the opening brace.
  3. Line break before the closing brace.
  4. Line break after the closing brace if that brace terminates a statement or the body of a function or class statement, or a class method. Specifically, there is no line break after the brace if it is followed by `else`, `catch`, `while`, or a comma, semicolon, or right-parenthesis.

Why: Because some browsers automatically standardize javacript code by supplementing “;” on each line of code.

Example:

```
class InnerClass {
  constructor() {}

  /** @param {number} foo */
  method(foo) {
    if (condition(foo)) {
      try {
        // Note: this might fail.
        something();
      } catch (err) {
        recover();
      }
    }
  }
}
```

Empty blocks: may be concise. An empty block or block-like construct may be closed immediately after it is opened, with no characters, space, or line break in between (i.e. `{}`), unless it is a part of a multi-block statement (one that directly contains multiple blocks: `if`/`else` or `try`/`catch`/`finally`).

Example:

```
function doNothing() {}
```
Illegal:
```
if (condition) {
  // …
} else if (otherCondition) {} else {
  // …
}

try {
  // …
} catch (e) {}

```

### **2.2. Block indentation: +2 spaces**

Each time a new block or block-like construct is opened, the indent increases by two spaces. When the block ends, the indent returns to the previous indent level. The indent level applies to both code and comments throughout the block. 

### 2.2.1. Class literals

Class literals (whether declarations or expressions) are indented as blocks. 

Do not add semicolons after methods, or after the closing brace of a class declaration (statements such as assignments that contain class expressions are still terminated with a semicolon). 
Use the extends keyword, unless the class extends a templatized type.

### 2.2.2. Function expressions

When declaring an anonymous function in the list of arguments for a function call, the body of the function is indented two spaces more than the preceding indentation depth.

### 2.2.3. Switch statements

The contents of a switch block are indented +2. After a switch label, a newline appears, and the indentation level is increased +2, exactly as if a block were being opened. An explicit block may be used if required by lexical scoping. The following switch label returns to the previous indentation level, as if a block had been closed.

Example:
```
switch (animal) {
  case Animal.BANDERSNATCH:
    handleBandersnatch();
    break;

  case Animal.JABBERWOCK:
    handleJabberwock();
    break;

  default:
    throw new Error('Unknown animal');
}

```

### **2.3. Statements**

- One statement per line. Each statement is followed by a line-break.
- Semicolons are required. Every statement must be terminated with a semicolon. Relying on automatic semicolon insertion is forbidden.
- Where to break? The prime directive of line-wrapping is: prefer to break at a `higher syntactic level`.

Preferred:
```
currentEstimate =
    calc(currentEstimate + x * currentEstimate) /
        2.0f;
```
Discouraged:
```
currentEstimate = calc(currentEstimate + x *
    currentEstimate) / 2.0f;
```
In the preceding example, the syntactic levels from highest to lowest are as follows: assignment, division, function call, parameters, number constant.

Operators are wrapped as follows:

  1. When a line is broken at an operator the break comes after the symbol.
  2. This does not apply to the dot (.), which is not actually an operator.
  3. A method or constructor name stays attached to the open parenthesis (`(`) that follows it.
  4. A comma (,) stays attached to the token that precedes it.


### **2.4. Comments**

### 2.4.1. Block comment style

Block comments are indented at the same level as the surrounding code. They may be in /* … */ or //-style. For multi-line /* … */ comments, subsequent lines must start with * aligned with the * on the previous line, to make comments obvious with no extra context. “Parameter name” comments should appear after values whenever the value and method name do not sufficiently convey the meaning.

```
/*
 * This is
 * okay.
 */

// And so
// is this.

/* This is fine, too. */

someFunction(obviousParam, true /* shouldRender */, 'hello' /* name */);

```
## **3. Language features**

JavaScript includes many dubious (and even dangerous) features. This section delineates which features may or may not be used, and any additional constraints on their use.

### **3.1. Local variable declarations**

### 3.1.1. Use const and let

Declare all local variables with either `const` or `let`. Use `const` by default, unless a variable needs to be reassigned. The `var` keyword must not be used.

### 3.1.2. One variable per declaration

Every local variable declaration declares only one variable: declarations such as `let a = 1, b = 2;` are not used.

### 3.1.3. Declared when needed, initialized as soon as possible

Local variables are not habitually declared at the start of their containing block or block-like construct. Instead, local variables are declared close to the point they are first used (within reason), to minimize their scope.

### **3.2. Array literals**

### 3.2.1. Do not use the variadic Array constructor

The constructor is error-prone if arguments are added or removed. Use a literal instead.

Illegal:

```
const a1 = new Array(x1, x2, x3);
const a2 = new Array(x1, x2);
const a3 = new Array(x1);
const a4 = new Array();
```
This works as expected except for the third case: if `x1` is a whole number then `a3` is an array of size `x1` where all elements are `undefined`. If `x1` is any other number, then an exception will be thrown, and if it is anything else then it will be a single-element array.

Instead, write
```
const a1 = [x1, x2, x3];
const a2 = [x1, x2];
const a3 = [x1];
const a4 = [];
```

Explicitly allocating an array of a given length using `new Array(length)` is allowed when appropriate.

### 3.2.2. Spread operator

Array literals may include the spread operator (`...`) to flatten elements out of one or more other iterables. The spread operator should be used instead of more awkward constructs with `Array.prototype`. There is no space after the `....`
Example:

```
[...foo]           // preferred over Array.prototype.slice.call(foo)
[...foo, ...bar]   // preferred over foo.concat(bar)

```
### **3.3. Object literals**
### 3.3.1. Do not use the Object constructor

While `Object` does not have the same problems as `Array`, it is still disallowed for consistency. Use an object literal (`{}` or `{a: 0, b: 1, c: 2}`) instead.

### 3.3.2. Do not mix quoted and unquoted keys

Object literals may represent either structs (with unquoted keys and/or symbols) or dicts (with quoted and/or computed keys). Do not mix these key types in a single object literal.

Illegal:

```
{
  a: 42, // struct-style unquoted key
  'b': 43, // dict-style quoted key
}
```

### 3.3.3. Computed property names

Computed property names (e.g., `{['key' + foo()]: 42}`) are allowed, and are considered dict-style (quoted) keys (i.e., must not be mixed with non-quoted keys) unless the computed property is a symbol (e.g., `[Symbol.iterator]`). Enum values may also be used for computed keys, but should not be mixed with non-enum keys in the same literal.

### **3.4. Enum**

Should give specific value to each enum member to keep it stable when the order changed.

Example:
```
public enum RequestStatus { 
  Pending = 1, 
  Approved = 2, 
  Delivered = 3 
}
```
### **3.5. Classes**

### 3.5.1. Constructors

Constructors are optional for concrete classes. Subclass constructors must call super() before setting any fields or otherwise accessing this. Interfaces must not define a constructor.


- The class keyword allows clearer and more readable class definitions than defining prototype properties.
- Do not manipulate prototypes directly. Mixins and modifying the prototypes of builtin objects are explicitly forbidden.
- The toString method may be overridden, but must always succeed and never have visible side effects. 
  
>> Tip: Beware, in particular, of calling other methods from toString, since exceptional conditions could lead to infinite loops.

### 3.5.2. Fields

Private fields must be annotated with @private and their names must start with a trailing underscore. 

Example: 

```
export class Foo {
  private _bar: string = 'something';
  
  constructor() {

  }
}
```

### 3.5.3. Static methods

Where it does not interfere with readability, prefer module-local functions over private static methods.

Static methods should only be called on the base class itself. Static methods should not be called on variables containing a dynamic instance that may be either the constructor or a subclass constructor, and must not be called directly on a subclass that doesn’t define the method itself.

Illegal:
```
export class Base { public static foo() {} }
export class Sub extends Base {}
function callFoo(cls) { cls.foo(); }  // discouraged: don't call static methods dynamically
Sub.foo();  // illegal: don't call static methods on subclasses that don't define it themselves

```
### **3.6. Functions**

### **3.7. Parameters**

### 3.7.1. Default parameters

Optional parameters are permitted using the equals operator in the parameter list. 

Optional parameters must include spaces on both sides of the equals operator, be named exactly like required parameters (i.e., not prefixed with opt_), use the = suffix in their JSDoc type, come after required parameters, and not use initializers that produce observable side effects. 

All optional parameters must have a default value in the function declaration, even if that value is undefined.

Example:
```
/**
 * @param {string} required This parameter is always needed.
 * @param {string=} optional This parameter can be omitted.
 * @param {!Node=} node Another optional parameter.
 */
public maybeDoSomething(required, optional = '', node = undefined) {}

```
### 3.7.2. Rest parameters

Use a rest parameter instead of accessing `arguments`. 

Rest parameters are typed with a `...` prefix.

The rest parameter must be the last parameter in the list. There is no space between the `...` and the parameter name.

Do not name the rest parameter `var_args`. Never name a local variable or parameter arguments, which confusingly shadows the built-in name.

Example:

```
/**
 * @param {!Array<string>} array This is an ordinary parameter.
 * @param {...number} numbers The remainder of arguments are all numbers.
 */
public variadic(array, ...numbers) {}

```
### 3.7.3. Returns

Function return types must be specified in the JSDoc directly above the function definition.

### **3.8. String literals**

### 3.8.1. Use single quotes

Ordinary string literals are delimited with single quotes (`'`), rather than double quotes (`"`).

### 3.8.2. Template strings

Use template strings (delimited with `) over complex string concatenation, particularly if multiple string literals are involved. Template strings may span multiple lines.

If a template string spans multiple lines, it does not need to follow the indentation of the enclosing block, though it may if the added whitespace does not matter.

Example:

```
public arithmetic(a, b) {
  return `Here is a table of arithmetic operations:
${a} + ${b} = ${a + b}
${a} - ${b} = ${a - b}
${a} * ${b} = ${a * b}
${a} / ${b} = ${a / b}`;
}
```
### 3.8.3. No line continuations

Do not use line continuations (that is, ending a line inside a string literal with a backslash) in either ordinary or template string literals. Even though ES5 allows this, it can lead to tricky errors if any trailing whitespace comes after the slash, and is less obvious to readers.

Illegal:

```
const longString = 'This is a very long string that far exceeds the 80 \
    column limit. It unfortunately contains long stretches of spaces due \
    to how the continued lines are indented.';

```
Instead, write:
```
const longString = 'This is a very long string that far exceeds the 80 ' +
    'column limit. It does not contain long stretches of spaces since ' +
    'the concatenated strings are cleaner.';

```
### **3.9. Number literals**

Numbers may be specified in decimal, hex, octal, or binary. Use exactly `0x`, `0o`, and `0b` prefixes, with lowercase letters, for hex, octal, and binary, respectively. Never include a leading zero unless it is immediately followed by `x`, `o`, or `b`.

### **3.10. Control structures**
### 3.10.1. For loops

With ES6, the language now has three different kinds of `for` loops. All may be used, though `for-of` loops should be preferred when possible.

- `for-in` loops may only be used on dict-style objects and should not be used to iterate over an array. Object.prototype.hasOwnProperty should be used in `for-in` loops to exclude unwanted prototype properties. 
- Prefer `for-of` and Object.keys over `for-in` when possible.

### 3.10.2. Exceptions

Exceptions are an important part of the language and should be used whenever exceptional cases occur. 

Always throw `Errors` or subclasses of `Error`: never throw string literals or other objects. Always use `new` when constructing an `Error`.

Custom exceptions provide a great way to convey additional error information from functions. They should be defined and used wherever the native Error type is insufficient.

### 3.10.3. Empty catch blocks

It is very rarely correct to do nothing in response to a caught exception. 

When it truly is appropriate to take no action whatsoever in a catch block, the reason this is justified is explained in a comment.

```
try {
  return handleNumericResponse(response);
} catch (ok) {
  // must write log to file
  // it's not numeric; that's fine, just continue  
}
return handleTextResponse(response);

```
Illegal:

```
try {
  shouldFail();
  fail('expected an error');
} catch (expected) {}
```

### 3.10.4. Switch statements

Terminology Note: Inside the braces of a switch block are one or more statement groups. Each statement group consists of one or more switch labels (either `case FOO:` or `default:`), followed by one or more statements.

### 3.10.4.1. The `default` case is present

Each switch statement includes a `default` statement group, even if it contains no code.

### 3.11. Disallowed features
### 3.11.1. with

Do not use the with keyword. It makes your code harder to understand and has been banned in strict mode since ES5.

### 3.11.2. Dynamic code evaluation

Do not use `eval` or the `Function(...string)` constructor (except for code loaders). These features are potentially dangerous and simply do not work in CSP environments.

### 3.11.3. Automatic semicolon insertion

Always terminate statements with semicolons (except function and class declarations).

### 3.11.4. Wrapper objects for primitive types

Never use `new` on the primitive object wrappers (`Boolean`, `Number`, `String`, `Symbol`), nor include them in type annotations.

Illegal:
```
const x = new Boolean(false);
if (x) {
  alert(typeof x);  // alerts 'object' - WAT?
}
```

The wrappers may be called as functions for coercing (which is preferred over using + or concatenating the empty string) or creating symbols.

Example:

```
const x = Boolean(0);
if (!x) {
  alert(typeof x);  // alerts 'boolean', as expected
}
```

### 3.11.5. Modifying built-in objects

Never modify built-in types, either by adding methods to their constructors or to their prototypes. Avoid depending on libraries that do this. Note that the JS Compiler’s runtime library will provide standards-compliant polyfills where possible; nothing else may modify built-in objects.

## **4. Naming**

### Pascal case

| Identifier                    | Example                                  |
| :---------------------------- | :--------------------------------------- |
| Namespace                     | Business.CustomerManagement              |
| Class                         | CustomerService                          |
| Interface and all its members | ICustomerService                         |
| Property                      | public string CustomerName { get; set; } |
| Method                        | GetAllCustomers()                        |
| Public static field           | public static int DefaultPort = 80;      |
| Event                         | ValueChange                              |
| Enum type, enum value         | Enum OrderStatus, OrderStatus.OutOfStock |

### Camel case
| Identifier                | Example                       |
| :------------------------ | :---------------------------- |
| Parameters                | GetCustomer(Guid customerId)  |
| Private fields            | private int amountOfProducts; |
| Variables inside a method | int totalDetailItems = 0;     |

### UPPERCASE

All letters in the identifier are capitalized. Use this convention only for identifiers that consist of `two` or fewer letters. For example: System.IO, UI…

- Do: DB, IO, UI, PC, GC, PO, EF…
- Don't: <del>HTTP, HTML, OLAP, ODBC, MVC, SQL…</del>

### **4.1. Rules common to all identifiers**

Identifiers use only ASCII letters and digits, and, in a small number of cases noted below, underscores.

Give as descriptive a name as possible, within reason. Do not worry about saving horizontal space as it is far more important to make your code immediately understandable by a new reader. 

### Abbreviations

- Do not use abbreviations that are ambiguous or unfamiliar to readers outside your project, and do not abbreviate by deleting letters within a word.
- Do avoid using class name duplicate that is used for namespace
- Do not use abbreviations or contractions as parts of identifier names. 

For example:
 
```
priceCountReader      // No abbreviation.
numErrors             // "num" is a widespread convention.
numDnsConnections     // Most people know what "DNS" stands for.

```

Illegal:
```
n                     // Meaningless.
nErr                  // Ambiguous abbreviation.
nCompConns            // Ambiguous abbreviation.
wgcConnections        // Only your group knows what this stands for.
pcReader              // Lots of things can be abbreviated "pc".
cstmrId               // Deletes internal letters.
kSecondsPerDay        // Do not use Hungarian notation.
```

If you must use abbreviations, use Camel Case for abbreviations that consist of more than two characters, even if this contradicts the standard abbreviation of the word. E.g. Html, Olap, DB, UI… 

Example: 

Use DbContext instead of <del>DBContext</del>

### **4.2. Rules by identifier type**

### 4.2.1. Package names

Package names are all `lowerCamelCase`.
For example:
```
`my.exampleCode.deepSpace`, but not `my.examplecode.deepspace` or `my.example_code.deep_space`.
```
### 4.2.2. Class names

Class, interface, record, and typedef names are written in `UpperCamelCase`. 

Type names are typically nouns or noun phrases. For example, `Request`, `ImmutableList`, or `VisibilityMode`. Additionally, interface names may sometimes be adjectives or adjective phrases instead (for example, `Readable`).

Use I prefix for interface. E.g: IStockService

### 4.2.3. Method names

Method names are written in `lowerCamelCase`. 

Method names are typically verbs or verb phrases. For example, `sendMessage`.

Getter and setter methods for properties are never required, but if they are used they should be named `getFoo` (or optionally `isFoo` or `hasFoo` for booleans), or `setFoo(value)` for setters.

### 4.2.4. Enum names

Enum names are written in `UpperCamelCase`, similar to classes, and should generally be singular nouns. 

Individual items within the enum are named in CONSTANT_CASE.

### 4.2.5. Constant names

Constant names use CONSTANT_CASE: all uppercase letters, with words separated by underscores.

Constants’ names are typically nouns or noun phrases. 

There is no reason for a constant to be named with a trailing underscore, since private static properties can be replaced by (implicitly private) module locals.

### 4.2.6. Non-constant field names

Non-constant field names (static or otherwise) are written in `lowerCamelCase`.

These names are typically nouns or noun phrases. For example, `computedValues` or `index`

### 4.2.7. Parameter names

Parameter names are written in `lowerCamelCase`. Note that this applies even if the parameter expects a constructor.

One-character parameter names should not be used in public methods.

### 4.2.8. Template parameter names

Template parameter names should be concise, single-word or single-letter identifiers, and must be all-caps, such as `TYPE` or `THIS`.

### 4.2.9. Camel case: defined

Sometimes there is more than one reasonable way to convert an English phrase into camel case, such as when acronyms or unusual constructs like IPv6 or iOS are present. To improve predictability, Google Style specifies the following (nearly) deterministic scheme.

Beginning with the prose form of the name:

1. Convert the phrase to plain ASCII and remove any apostrophes. For example, Müller's algorithm might become Muellers algorithm.
2. Divide this result into words, splitting on spaces and any remaining punctuation (typically hyphens).
a. Recommended: if any word already has a conventional camel case appearance in common usage, split this into its constituent parts (e.g., AdWords becomes ad words). Note that a word such as iOS is not really in camel case per se; it defies any convention, so this recommendation does not apply.
3. Now lowercase everything (including acronyms), then uppercase only the first character of:
  a. … each word, to yield upper camel case, or
  b. … each word except the first, to yield lower camel case
4. Finally, join all the words into a single identifier.
 
Note that the casing of the original words is almost entirely disregarded.

Examples:

|Prose form |	Correct |	Incorrect |
|:----------|:--------|:-----------|
|XML HTTP request	| XmlHttpRequest	| XMLHTTPRequest|
|new customer ID	| newCustomerId	| newCustomerID|
|inner stopwatch |	innerStopwatch |	innerStopWatch|
|Supports IPv6 on iOS?	| supportsIpv6OnIos |	supportsIPv6OnIOS|
|YouTube importer	| YouTubeImporter |	YoutubeImporter*|

*Acceptable, but not recommended.

> Note: Some words are ambiguously hyphenated in the English language: for example `nonempty` and `non-empty` are both correct, so the method names `checkNonempty` and `checkNonEmpty` are likewise both correct.

## **5. Policies**

### **5.1. Be consistent**

 For any style question that isn't settled definitively by this specification, prefer to do what the other code in the same file is already doing. 
 
 If that doesn't resolve the question, consider emulating the other files in the same package.

 `#region` and `#endregion` should be used to group code.

### **5.2. Compiler warnings**

### 5.2.1. Use a standard warning set

As far as possible projects should use --warning_level=`VERBOSE`.

### 5.2.2. How to handle a warning

Before doing anything, make sure you understand exactly what the warning is telling you. If you're not positive why a warning is appearing, ask for help .

Once you understand the warning, attempt the following solutions in order:
1. `First, fix it or work around it`. Make a strong attempt to actually address the warning, or find another way to accomplish the task that avoids the situation entirely.
2. `Otherwise, determine if it's a false alarm`. If you are convinced that the warning is invalid and that the code is actually safe and correct, add a comment to convince the reader of this fact and apply the suppress annotation.
3. Otherwise, leave a `TODO comment`. This is a `last resort`. If you do this, `do not suppress the warning`. The warning should be visible until it can be taken care of properly.

### 5.2.3. Suppress a warning at the narrowest reasonable scope

Warnings are suppressed at the narrowest reasonable scope, usually that of a single local variable or very small method. Often a variable or method is extracted for that reason alone.
