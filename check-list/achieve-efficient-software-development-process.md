###  Sprint - meeting : 
1. In this meeting we will be deciding all possible features which can be done in upcoming sprint.
2. Discussion will be whole day. Where we have to decide all possibilities. 
3. Estimate task and decided UI for app. If there is any issue in UI this is the time to point out the mistakes
4. If there is any issue in API this is the time to point out the issues.
5. If there is any R&D task, please mention and estimate the R&D task.

### How to estimate any task?
1. Think about the efforts and possibilities. Its not easy to estimate but it comes only with experience. So keep estimating on regular basis one day you will reach there.
2. Check for existing, if any. To save time and efforts. (Please refer to question how to choose Third party lib)
3. Break estimations into UI, Validations, test cases and API call.
4. Try to give some category to the estimation. Like how much effort will it take Simple, Intermediate, Complex.
5. What if task is simple but it will take 6hrs to do. Some task are time consuming task. Mention on trello card its simple task but time consuming. 
6. Not all time consuming task are complex and not all complex task are time consuming. Complexity is defined on architecture and efforts
7. Dont forget to estimate time for beta testing. This testing is on device not on emulator.

### Scrum works
1. The scrum will last for 120hrs.
2. First 80hrs are for development and next 40hrs is for testing and bug fixing.
3. Do not mark something done if the task is really not done. If you think its done and there is one minor issue for that task, add one more checklist on that card. For e.g : Login screen. Now in this card add Checklist - task. If you think there is bug which belongs to this card. Add checklist - bug and work on that bug. Only once you fix that bug add the card in review. 
4. When you add card in review make commit. In this case fixed login and login related bugs. You can create github issue for bugs and add in commit message like this : git commit -m "Login issue fixed #31 #12"
5. Best practice is whenever you start new card make new branch related to that card and send PR once its done. Assign the PR(Pull request) to reviewer. 
6. Once card is done. PR is sent. Ask UI reviewer to review the UI. In this case bump one version of the app and send build to tester. Use fabric - crashlytics.
7. Only once both UI reviewer and PR reviewer approves only then merge the PR. Only then the card will be shifted to done list.
8. You can move ahead with task if review is pending. But this card will be in review. If any bugs are reported you have to fix it in 3rd week of sprint which is last week of sprint of bug fixing.
9. Development will speed up and work smoothly only and only if we follow these rules. 
10. If you are going to take leave. Inform that in sprint meeting so that planning is not harmed.

### Donts
1. Do not assume things. If you are in doubt communicate and solve the issue.
2. If you are working from home. Dont procrastinate its not going to help you. You will end up working whole day.
3. If you are not comfortable with estimation please communicate it sprint meeting.
4. If you are stuck dont waste lot of time fixing that one issue. Remember you have time to fix issue in last week of sprint as well.

### How to choose third party library?
1. Check when was the last commit by the library contributor.
2. Check how many stars are there on github.
3. Check number of contributors.
4. Check how dedicated is team and how often they provide support for new OS version.
5. Check if the license it should be open source license.
6. How much time will be spent customizing a third-party library to satisfy all of the requirements for your functionality?
7. Does your team plan to expand this function in the future?
8. If so, will this library allow room for that expansion?
9. Does a Google or StackOverflow search yield positive comments and questions with definite answers, or does the community seem to be struggling with this library’s implementation?
10. If the project needed to move away from this library in the future, how much code would have to be discarded?
11. How will this library be affected by OS updates? Does this library try to replace large portions of system framework functionality like Core Data or Android Fragments?
12. How difficult is it to set up your development environment for the first time?