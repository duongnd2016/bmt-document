# Note

This is not a checklist for *approving* or *merging* code, it is a checklist for *reviewing* code.    

# Git workflows

    brand master
    brand develop
    brand feature (many)

    master->develop:

    develop->feature: clone local and push to origin
    loop commit
        feature->feature: commit 1 ...
        feature->feature: commit 2 ...
        feature->feature: commit 3 ...
    end
   
    feature->develop: pull merge request and ask for review 
    feature: review checklist & feedback from reviewer as comments in gitlab
    feature->develop: ask for review 
   
    develop->master => QC test or production

# Git guide lines
- Commit early and often on local repository
- Keep features/code reviews small, divided into logical parts
- Commits messages are small and understandable
- Make sure no dist files, editor/IDE files, etc are checked in. There should be a .gitignore for that


# PR Submission checklist BEFORE code review

- [ ] A JIRA ticket is mentioned (or not needed)
- [ ] *ONE* JIRA ticket => *ONE* feature-branch 
- [ ] Follow naming brand convention => feature/#12345_the-description-of-the-feature

# Self review

- [ ] I've checked there is appropriate unit test coverage.
- [ ] I've checked there is appropriate selenium test coverage.
- [ ] I've included regression tests for any bug fixes.
- [ ] I've updated documentation with any changes to procedures.
- [ ] I've tested this on mobile Android, Safari, Chrome for any visual changes.
- [ ] I've tested this on desktop IE9/10/11, Firefox, Chrome for any visual changes.
- [ ] I've checked any significant visual changes with a member of the UX/UI pod.
- [ ] I've checked this work against the requirements of the JIRA.
  - [ ] Meet all requirements of the user-story ?
  - [ ] Which part of code solves an reqirement ?
- [ ] I've added and updated translations for all supported languages
- [ ] I've informed those who need to know of any unforeseen impact of this work (database migrations, deployment procedure changes).
- [ ] I am ready for this to be code reviewed, merged and tested.
  - [ ] I've witnessed the work behaving as expected.
  - [ ] I've run all the tests locally and they pass.

# Integration  

- [ ] Is there a clear and concise description of the changes?
- [ ] Will merging this code create source conflicts?  
- [ ] Did all automated checks (build, test, lint) run and pass?
- [ ] Code compiles without errors or warnings ?  
- [ ] Debug print/log statements have been removed
- [ ] Unsued, redundant or duplicate code and commented out code has been removed ?
- [ ] Removed unused packages (NPM)
- [ ] Are there supporting metrics or reports (e.g. test coverage, fitness functions) that measure the impact?
- [ ] Are there obvious logic errors or incorrect behaviors that might break the software?
- [ ] Performance analysis reports no leaks or general performance issues
- [ ] Can any of the code be replaced with library functions?
- [ ] Correct member accessor conventions (private, protected, public)

# Testing

- [ ] Is the code testable, maintainable, trustworthy?
- [ ] Test names (describe, it) are concise, explicit, descriptive
- [ ] Use a mock to simulate/stub complex class structure, methods or async functions
- [ ] Do tests exist and are they comprehensive?
- [ ] Do unit tests actually test that the code is performing the intended functionality?
- [ ] Don't test multiple concerns in the same test
- [ ] Cover the general case and the edge cases
- [ ] Could any test code be replaced with the use of an existing API?

# Readability

- [ ] Code formatted according to team style guide
- [ ] Is the code self-documenting? Do we need secondary sources to understand it?  
- [ ] Do the names of folders, objects, functions, and variables intuitively represent their responsibilities?  
- [ ] Could comments be replaced by descriptive functions?  
- [ ] Is there an excessively long object, method, function, pull request, parameter list, or property list? Would decomposing make it better? .  
- [ ] Avoid multiple if/else blocks (should use return first approach)
- [ ] Is the use and function of third-party libraries documented?
- [ ] Are data structures and units of measurement explained?
- [ ] Is there any incomplete code? If so, should it be removed or flagged with a marker like ‘TODO’?

# Anti-patterns

- [ ] Does the code introduce any of the following anti-patterns?
- [ ] Sequential coupling - a class that requires its methods to be called in order  
- [ ] Circular dependency - mutual dependencies between objects or software modules  
- [ ] Shotgun surgery - a change needs to be applied to multiple classes at the same time  
- [ ] Magic numbers - unexplained numbers in algorithms  
- [ ] Hard code - embedding assumptions about the environment in the implementation  
- [ ] Error hiding - catching an error and doing nothing or showing a meaningless message  
- [ ] Feature envy - a class that uses methods of another class excessively  
- [ ] Duplicate code - identical or very similar code exists in more than one location.  
- [ ] Boat anchor - retaining a part of a system that no longer has any use  
- [ ] Cyclomatic complexity - a function contains too many branches or loops 
- [ ] Famous volatility - a class or module that many others depend on and is likely to change 
  
# Design principles

- [ ] SOLID
  - [ ] Single Responsibility - an object should have only one reason to change  
  - [ ] Open/Closed - objects should be open for extension, closed for modification  
  - [ ] Liskov Substitution - subtypes should not alter the correctness of code that depends on a supertype  
  - [ ] Interface Segregation - many client specific interfaces are better than one general purpose interface  
  - [ ] Dependency Inversion - dependencies should run in the direction of abstraction; high level policy should be immune to low level details
- [ ] KISS (Keep it simple, stupid) - stupid functions don't know about referrer/environment
- [ ] DRY (Don't repeat yourself)
  - [ ] Use template-partials to prevent duplicate markup
  - [ ] Use functions to call multiple code-snippets
- [ ] CGL (PSR -CodingGuideLines)
  - [ ] Camel-case-written, readable functions and variables
  - [ ] Domain-driven ubiquitous Language
- [ ] YAGNI (You Aren't Gonna Need It)
  - [ ] Should not add functionality until deemed necessary
  - [ ] Should integrate existing libraries before reinventing the wheel

# Security

- [ ] Are all data inputs checked (for the correct type, length, format, and range) and encoded?
- [ ] Where third-party utilities are used, are returning errors being caught?
- [ ] Are output values checked and encoded?
- [ ] Are invalid parameter values handled?
  
  
# Reviewer

- [ ] I agree with the assumptions made in the quality checklist.
- [ ] I've witnessed the work behaving as expected.
- [ ] I've run all the tests locally and they pass.
- [ ] I've checked this work against the requirements of the JIRA.
- [ ] I've checked for appropriate test coverage.
- [ ] I've checked for coding anti-patterns.
- [ ] Technical impact look okay.
- [ ] +1 from me!

| comment tiers | comment                                    | deployment |
|:--------------|--------------------------------------------|:----------:|
| you might     | better comments, ubiquitouseLanguage, KISS | O (yes)    |
| you should    | CGL, DRY, YAGNI, SRP                       | O (yes)    |
| you must      | bug, insecure, lack of validation          | X (no)     |