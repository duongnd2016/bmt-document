# UI Code Review Checklist

The following checklist is a checklist for UI code reviews. It is useful for developers to reference when doing a UI code review.

## Generals

* [ ] Site uses a cache buster for expiring .js, .css, and images
* [ ] JavaScript and CSS is minified and concatenated into logical groupings
* [ ] Images size have been optimized

## Codestyle

* [ ] No hardcoded values, use constants values.
* [ ] Avoid multiple if/else blocks.
* [ ] No commented out code.
* [ ] No unnecessary comments: comments that describe the how.
* [ ] Add necessary comments where needed. Necessary comments are comments that describe the why.

## ESLint

* [ ] Code has no any linter errors or warnings.
* [ ] No console.logs.

## ES6/7

* [ ] 100% passing unit test coverage.
* [ ] Passes all linting checks.
* [ ] Use fat arrow instead of var that = this. Be consistent in your usage of arrow function.
* [ ] Use spread/rest operator.
* [ ] All naming conventions are followed.
  * [ ] All Classes are Pascal case. 
  * [ ] All Class files are Pascal case.
  * [ ] All Class members are Camel case.
  * [ ] All directories are Camel case.
  * [ ] Correct member accessor conventions (_private, $protected, public)
* [ ] One Class per file.
* [ ] Use const over let (avoid var).
* [ ] Use import and export.
* [ ] Use Promises or Asyns/Await. Rejection is handled.
* [ ] Use template literals.
* [ ] No circular dependencies.
* [ ] Favors destructors. Use destructuring assignment for arrays and objects.
* [ ] No null reference exceptions.
* [ ] Code is correctly formatted.
* [ ] No redundant or extraneous code. (e.g. unnecessary or commented out)
* [ ] All expected error scenarios are covered and accounted for.
* [ ] All unexpected error scenarios are covered and accounted for. (e.g. downed services; malformed API responses; null, undefined, or empty values; incorrect types)


## Markups

* [ ] All naming conventions are followed.
* [ ] Code is correctly formatted. (e.g. attributes, nesting)
* [ ] Code does not contain inline JavaScript event listeners
* [ ] Code does not contain inline style attributes
* [ ] Code does not contain deprecated elements & attributese
* [ ] Page begins with a valid DTD (HTML5 doctype)
* [ ] Code is indented using hard tabs
* [ ] Tags and attributes are lowercase
* [ ] Tags are closed and nested properly
* [ ] Markup is semantic (e.g. class names do not denote presentation, Items in list form are housed in a UL, OL, or DL)
* [ ] Tables are only used to display tabular data
* [ ] Element IDs are unique
* [ ] Code validates against the W3C validator
* [ ] DOM nesting depth does not exceed 12 levels
* [ ] Total page weight does not exceed client requirements (e.g. 1000kb)
* [ ] Title case is used for headers/titles and forced to all caps using the CSS declaration text-transform: uppercase;
* [ ] Where text is included via images, CSS image replacement is used.

## CSS

* [ ] Consistent naming conventions are used (BEM, OOCSS, SMACSS, e.t.c.).
* [ ] All selectors Kebob case.
* [ ] Only class and pseudo selectors. (No tag, id, name, etc.)
* [ ] Only one level of nesting.
* [ ] Code is correctly formatted.
* [ ] Adheres to correct units.
* [ ] No redundant or extraneous code. (e.g. unnecessary or commented out)
* [ ] Style blocks are externalized to .css files 
* [ ] A print-friendly .css file is included in the page
* [ ] A reset.css file is included in the page
* [ ] CSS validates against the W3C validator
* [ ] CSS selectors are only as specific as they need to be; grouped logically 
* [ ] CSS sprites are used to combine images
* [ ] CSS selectors gets more specific across files
* [ ] CSS shorthand is used for properties that support it
* [ ] CSS selectors are not tag qualified
* [ ] CSS properties are alphabetical (except for vendor-specific properties) 
* [ ] CSS selectors are only as specific as they need to be; grouped logically.
* [ ] Is any 'CSS in JS' library was used?
* [ ] Use Hex color codes #000 unless using rgba().
* [ ] Avoid absolute positioning.
* [ ] Use flexbox.
* [ ] Avoid !important.
* [ ] Do not animate width, height, top, left and others. Use transform instead.
* [ ] Use same units for all project.
* [ ] Avoid inline styles.

## React

* [ ] All ES6 review passing.
* [ ] No data direction reversals.

##Forms

* [ ] All user input is “sanitized”
* [ ] Form elements are paired with labels elements containing the for attribute
* [ ] Form label/input pairs are wrapped with a block level element (e.g. `<p>`)
* [ ] Forms are logically arranged within fieldsets

##Accessibility

* [ ] Page has a proper outline (H1-H6 order)
* [ ] Alt attributes exist on all <img> elements
* [ ] Video is accompanied by a transcript and closed captioning
* [ ] Code validates against WCAG priority level 1 and 2
* [ ] Events and styles applied to :hover are also applied to :focus
* [ ] Tabindex order is logical and intuitive

##JavaScript

* [ ] Script blocks are externalized to .js files
* [ ] Consistent naming conventions are used
* [ ] Code adheres to the K&R style
* [ ] Core page features function with JavaScript disabled
* [ ] JavaScript belongs to a namespace (no globally scoped code)
* [ ] jQuery selectors are performant
* [ ] jQuery objects are cached
* [ ] Event delegation is used for binding events to 2 or more elements, or ajax'd elements
* [ ] Script blocks are placed before the closing <body> tag
* [ ] Code has been run through JSLint (jslint.com) or JSHint (jshint.com)

##SEO

* [ ] Uses a valid <title> element with a valid text node
* [ ] Uses description meta data
* [ ] Uses visible header tags


##Code Review Tools

  1. Markup: DOM Monster bookmarklet
  2. Markup: Diagnose Chrome Extension
  3. Markup: W3C HTML validator
  4. CSS: W3C CSS validator
  5. Performance: YSlow
  6. Performance: PageSpeed
  7. JavaScript: JSLint/JSHint
  8. Accessibility: SortSite
  9. Accessibility: JAWS/NVDA/VoiceOver
  10. Accessibility: CynthiaSays
